package com.latihan;

import com.latihan.controller.HelloSpringControllerConstructor;
import com.latihan.controller.HelloSpringControllerSetter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class LatihanApplication 
{
    public static void main(String[] args) 
    {
        ApplicationContext context = SpringApplication.run(LatihanApplication.class, args);
        HelloSpringControllerConstructor helloConstructor = (HelloSpringControllerConstructor) context.getBean("helloSpringControllerConstructor");
        helloConstructor.getMessage();
        
        HelloSpringControllerSetter helloSetter = (HelloSpringControllerSetter) context.getBean("helloSpringControllerSetter");
        helloSetter.getMessage();
    }
}
