/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.latihan.controller;

import com.latihan.service.HelloSpringServie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author USER
 */
@Controller
public class HelloSpringControllerConstructor 
{
    private HelloSpringServie helloService;
    
    //inject by constructor
    
    @Autowired
    public HelloSpringControllerConstructor(HelloSpringServie helloSpringServie)
    {
        this.helloService = helloSpringServie;
    }
    
    public void getMessage()
    {
        helloService.sayHello();
    }
}
