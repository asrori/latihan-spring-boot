/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.latihan.dao;

import com.latihan.service.HelloSpringServie;
import org.springframework.stereotype.Component;

/**
 *
 * @author USER
 */
@Component
public class HelloSpringImp implements HelloSpringServie
{
    @Override
    public void sayHello() 
    {
        System.out.println("Hello Spring Boot");
    }
    
}
